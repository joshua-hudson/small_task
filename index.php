<!DOCTYPE html>
<!-- This php uses the plugin geoplugin to get the users location and returns that information in the form of an array -->
<?php
  //grabbing the users ip and running it through the plugin
  $geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SERVER['REMOTE_ADDR']));
  //picking out the information in the returned array into variables
  $country = $geo['geoplugin_countryName'];
  $symbol = $geo['geoplugin_currencySymbol'];
  $rate = $geo['geoplugin_currencyConverter'];
  //setting the price of the product here for an example
  $price = 24.99;
  //converting the price so that the correct amount is changed depending on where the user is located
  $converted_price = $price * $rate;
  //formating the convered price
  $converted_price_formatted = number_format($converted_price,2);
  //An array of countries with their delevery charges
  $delievery_charges = array(
    'United Kingdom' => 10.00 ,
    'United States' => 10.99,
    'France' => 4.99,
    'Germany' => 3.99,
  );
  $test = null;
 ?>

<html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/master.css">
    <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="js/master.js"></script>
    <meta charset="utf-8">
    <title>Project Azir</title>
  </head>

  <body>
    <nav>
      <div class="row fix">
        <div class="col-lg-offset-2 col-lg-2 col-xs-6 col-xs-offset-3">
          <img src="images/logo.png" alt="logo">
        </div>

      </div>
    </nav>

    <hr>
    <div class="payment col-lg-8 col-lg-offset-2" >
      <ul class="payment_steps">
        <li class="success">Sign In</li>
        <li class="success">Delievery </li>
        <li><strong>|Payment|</strong></li>
        <li>Verify and Confirm</li>
      </ul>
      <hr>

    <!-- BASKET SECTION -->

  <div class="row fix">
    <div class="col-lg-8 col-lg-offset-2  basket">
        <h1>Order Summary</h1>
        <table class="table">
          <th>Product</th>
          <th>Price</th>
          <th>Quantity</th>

          <tr>
            <td>Item number 1</td>
            <?php
              //INPUTTING THE PRICE INTO THE TABLE
              echo "<td>$symbol$converted_price_formatted</td>";
             ?>
            <td>
              <select class="" name="">
                <option value="">1</option>
                <option value="">2</option>
                <option value="">3</option>
              </select>
            <td>
          </tr>
          <tr>
            <td>Delivery</td>
            <td>
              <?php
              //FOR EACH LOOP THAT LOOPS THROUGH EACH DELIEVERY CHARGE AND SETS IT TO THE CORRECT ONE
                foreach ($delievery_charges as $charge => $value) {

                  if ($country == $charge) {
                    echo $symbol.  Number_format($value, 2).' ';
                    $test = $value;
                  }
                }
               ?>
            </td>
            <td></td>
          </tr>

          <tr>
            <td>Total</td>
            <td>
              <?php
                //TOTAL SUM OF THE ITEMS PLUS THE DLEIEVERY CHARGE
                $total = $converted_price_formatted + $test;
                echo $symbol . $total;
               ?>
            </td>
            <td></td>
          </tr>
        </table>
    </div>
  </div>


  <!-- THE THREE PAYMENT OPTIONS-->
  <div class="row fix">
    <div class="col-lg-4 col-xs-12">
      <button class="payment_buttons " id="card_button" >Card Payment</button>
    </div>

    <div class="col-lg-4 col-xs-12">
      <button class="payment_buttons" id="paypal_button" >Paypal</button>
    </div>

    <div class="col-lg-4 col-xs-12">
      <button class="payment_buttons" id="alipay_button" >Alipay</button>
    </div>

  </div>
<div class="row fix">
  <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">

    <!-- CARD PAYMENT FORM -->

    <form id="card_form" class="" action="index.html" method="post">
      <h3>Card Payment</h3>
      <div class="row fix">
        <label  for="1"class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Name on Credit/Debit Card * </label>
        <input class="col-lg-6 col-md-6 gap" type="text" name="1" placeholder="XXXX-XXXX-XXXX-XXXX" >
      </div>

      <div class="row fix">
        <label  for="1" class="col-lg-6 col-md-6 col-sm-6">Card Type *</label>
        <select class="gap" name="" class="col-lg-6 col-md-6 col-sm-4">
          <option value="">Visa</option>
          <option value="">MasterCard</option>
          <option value="">AMEX</option>
          <option value="">Maestro</option>
        </select>
      </div>

      <div class="row fix">
        <label for="1" class=" col-lg-6 col-md-6 col-sm-6">Experation Date *</label>
        <select class="gap col-lg-2 col-md-2 col-sm-4 select_width" name="">
          <option value="">01</option>
          <option value="">02</option>
          <option value="">03</option>
          <option value="">04</option>
          <option value="">05</option>
          <option value="">06</option>
          <option value="">07</option>
          <option value="">08</option>
          <option value="">09</option>
          <option value="">10</option>
          <option value="">11</option>
          <option value="">12</option>
        </select>

        <select class=" gap col-lg-2 col-md-2  select_width" name="">
          <option value="">2017</option>
          <option value="">2018</option>
          <option value="">2019</option>
          <option value="">2020</option>
        </select>
      </div>

      <div class="row fix">
        <label for="1" class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Security Code *</label>
        <input class="gap col-lg-6 col-md-6" type="text" name="" value="" placeholder="Security Code*">
      </div>

      <div class="row fix">
        <label  for="1" class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Issue Number <br> (If applicable)</label>
        <input type="text" name="1" value="" class=" col-lg-6 col-md-6" placeholder="Issue Number (If Needed)">
      </div>

      <div class="row fix">
        <p>Billing address the same as the delievery address?</p>
          <div class="row">
            <div class="col-lg-12">
              <input checked="checked" id="delievery_address" type="radio" name="billing" value="Yes" class="col-lg-1 col-md-1 col-xs-2 radio_button">
              <label for="Yes" class="radio_label ">Yes</label>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
              <input id="billing_address" type="radio" name="billing" value="No" class="col-lg-1 col-md-1 col-xs-2 ">
              <label for="No" class="radio_label">No</label>
            </div>
          </div>




          <!--BILLING ADDRESS FORM-->
          <div id="billing_address_form">
            <h3>Billing Address</h3>
            <div class="row fix">
              <label for="1"class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Title*</label>
              <select class="gap col-md-5" name="">
                <option value="">Mr</option>
                <option value="">Mrs</option>
                <option value="">Miss</option>
              </select>
            </div>

            <div class="row fix">
              <label for="1"class=" col-lg-6 col-md-6 col-sm-6 hidden_label" >First Name*</label>
              <input class="gap col-lg-6 col-md-6" type="text" name="" placeholder="First Name*">
            </div>

            <div class="row fix">
              <label for="1"class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Last Name*</label>
              <input class="gap col-lg-6 col-md-6" type="text" name="" placeholder="Last Name*">
            </div>

            <div class="row fix">

              <?php
              //IF STATEMENT TO RUN THROUGH WHICH COUNTRY THE USER IS LOCATED AND SWITCHES TO THE RELEVANT POSTCODE OR ZIPCODE
              if ($country == 'United Kingdom') {
                echo "<label for='1'class=' col-lg-6 col-md-6 col-sm-6 hidden_label'>Postcode*</label>";
                echo "<input class='gap col-lg-6 col-md-6 col-sm-6' type='text' name='' placeholder='Postcode*'>";
              }elseif ($country == 'United States') {
                echo "<label for='1'class=' col-lg-6 col-md-6 col-sm-6 hidden_label'>United States*</label>";
                echo "<input class='gap col-lg-6 col-md-6 col-sm-6' type='text' name='' placeholder='United States*'>";
              }
              ?>

            </div>

            <div class="row fix">
              <label for="1"class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Address First Line*</label>
              <input class="gap col-lg-6 col-md-6 " type="text" name="" placeholder="Address Line One*">
            </div>

            <div class="row fix">
              <label for="1"class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Address Second Line</label>
              <input class="gap col-lg-6 col-md-6 " type="text" name="" placeholder="Address Line Two">
            </div>

            <div class="row fix">
              <label for="1"class=" col-lg-6 col-md-6 col-sm-6 hidden_label">Country</label>
              <select class="gap col-lg-4 col-md-4 col-sm-4 col-xs-12" name="">
                <option value="">United Kingdom</option>
                <option value="">United States</option>
                <option value="">Germany</option>
                <option value="">France</option>
                <option value="">Spain</option>
                <option value="">China</option>
              </select>
            </div>

          </div>


          <button type="button" name="button" class="checkout_button">Checkout</button>

        </form>
      </div>

      <!--PAYPAL AND ALIPAY PAYMENT OPTIONS-->

      <div id="paypal">
        <div class="row">
          <div class="col-lg-6 col-lg-offset-3">
            <img src="images/paypal.png" alt="">
          </div>
        </div>
        <strong>You have chosen to pay by PayPal, Please note that using the PayPal checkout will attract a 1% surcharge to the cost of your order.</strong>
        <button type="button" name="button">Checkout</button>
      </div>

      <div id="ali_pay">
        <div class="row">
          <div class="col-lg-6 col-lg-offset-3">
            <img id="alipay_pic" src="images/alipay.png" alt="">
          </div>
        </div>
        <strong>You have chosen to pay via Alipay, please click check out to finialize your payment.</strong>
        <button type="button" name="button">Checkout</button>
      </div>

    </div>
  </div>
 </div>
</div>
  </body>
</html>
