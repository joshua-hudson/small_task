$(document).ready(function(){
  $(paypal).hide();
  $(ali_pay).hide();
  $(billing_address_form).hide();

  $(card_button).click(function () {
    $(paypal).slideUp(500);
    $(ali_pay).slideUp(500);
    $(card_form).delay(500).slideDown(500);
    $(billing_address_form).slideUp(500);
  });

  $(paypal_button).click(function () {
    $(card_form).slideUp(500);
    $(ali_pay).slideUp(500);
    $(paypal).delay(500).slideDown(500);
    $(billing_address_form).slideUp(500);
  });

  $(alipay_button).click(function functionName() {
    $(card_form).slideUp(500);
    $(paypal).slideUp(500);
    $(ali_pay).delay(500).slideDown(500);
    $(billing_address_form).slideUp(500);
  });

  $(delievery_address).click(function(){
    $(billing_address_form).slideUp(500);
  });

  $(billing_address).click(function () {
    $(billing_address_form).slideDown(500);
  })


      });
